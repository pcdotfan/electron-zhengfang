package com.pcdotfan.zhengfang.exception;

public class AuthenticationFailedException extends RuntimeException {

    public AuthenticationFailedException(String exception) {
        super(exception);
    }

}
