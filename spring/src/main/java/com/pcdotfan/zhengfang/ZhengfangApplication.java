package com.pcdotfan.zhengfang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZhengfangApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZhengfangApplication.class, args);
	}
}
