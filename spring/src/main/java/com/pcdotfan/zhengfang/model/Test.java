package com.pcdotfan.zhengfang.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "test")
public class Test {

    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String name; // slug

    @Column()
    private String title; // 实际显示标题

    @Column
    private long studentId;

    @Column
    private float score;

    private Date created;
    private Date updated;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public long getStudentId() {
        return studentId;
    }
    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public float getScore() {
        return score;
    }
    public void setScore(float score) {
        this.score = score;
    }

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }
}
