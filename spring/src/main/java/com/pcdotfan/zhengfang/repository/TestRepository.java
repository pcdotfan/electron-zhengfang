package com.pcdotfan.zhengfang.repository;

import com.pcdotfan.zhengfang.model.Test;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRepository extends JpaRepository<Test, Long> {
}