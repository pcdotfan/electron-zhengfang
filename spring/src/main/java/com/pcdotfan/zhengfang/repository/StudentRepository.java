package com.pcdotfan.zhengfang.repository;

import com.pcdotfan.zhengfang.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {

}