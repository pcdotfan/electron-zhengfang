import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import interopFallback from './interop-fallback'

import AsyncComputed from 'vue-async-computed'
import router from './router'

import './element-ui'

import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'

UIkit.use(Icons)
Vue.use(AsyncComputed)

Vue.config.productionTip = false

axios.defaults.baseURL = 'http://localhost:8080/api'

Vue.prototype.$http = axios

const interop = window.interop || interopFallback
Vue.prototype.$interop = interop
Vue.prototype.$log = interop.log
Vue.$log = interop.log

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
